FROM debian:buster

# Install packages
ENV DEBIAN_FRONTEND noninteractive

RUN mkdir /app
WORKDIR /app

ADD ["capanalysis_1.2.3_amd64.deb", "/app"]
RUN apt update
RUN apt upgrade -y
RUN apt install gdebi -y
RUN apt install wget -y

RUN echo '#!/bin/sh' > /usr/sbin/policy-rc.d \
    && echo 'exit 101' >> /usr/sbin/policy-rc.d \
    && chmod +x /usr/sbin/policy-rc.d
# ENV DEBIAN_FRONTEND noninteractive
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tshark
RUN gdebi --n /app/capanalysis_1.2.3_amd64.deb
RUN sed -i -e 's/PRIORITY=1 #(0..20)/PRIORITY=0 #(0..20)Z/g' /etc/init.d/capanalysis
RUN apt install sudo
ADD upload.ini /etc/php/7.3/apache2/conf.d


CMD sudo service postgresql restart && \
sudo service apache2 restart && \
sudo service capanalysis restart && \
sudo service smbd restart && \
sudo service nmbd restart && \
tail -f /var/log/apache2/access.log

VOLUME /var/lib/postgresql
VOLUME /opt/capanalysis
EXPOSE 9877

RUN apt install samba
RUN useradd capanalysis
RUN echo 'capanalysis:capanalysis' | chpasswd
RUN  (echo "capanalysis"; echo "capanalysis") | smbpasswd -s -a "capanalysis"
RUN echo "[capanalysis]" >> /etc/samba/smb.conf
RUN echo "comment = capanalysis share" >> /etc/samba/smb.conf
RUN echo "path = /opt/capanalysis" >> /etc/samba/smb.conf
RUN echo "read-only = no" >> /etc/samba/smb.conf
RUN echo "guest ok = yes" >> /etc/samba/smb.conf
RUN echo "browsable = yes" >> /etc/samba/smb.conf
RUN echo "valid users = capanalysis" >> /etc/samba/smb.conf
RUN echo "guest account = capanalysis" >> /etc/samba/smb.conf
RUN echo "create mask = 0775" >> /etc/samba/smb.conf
RUN echo "directory mask = 0755" >> /etc/samba/smb.conf

EXPOSE 445
EXPOSE 139
EXPOSE 138
EXPOSE 137

#ENTRYPOINT [ "/bin/sh" ]
