# capanalysis-docker
Dockerfile to build and run capanalysis


## Build

To build (locally) the image just do

```sh
docker build . -t capanalysis # build the image
```

## Run

To execute the image (in daemon mode) do:

```sh
docker run -d --name capanalysis -p 9877:9877/tcp -p 137:137/tcp -p 138:138/tcp -p 139:139/tcp -p 445:445/tcp --restart unless-stopped capanalysis:latest
```

## Data

To manage data use samba by connecting to \\127.0.0.1\capanalysis

user: capanalysis
password: capanalysis